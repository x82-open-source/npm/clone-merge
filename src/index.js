import isArray from 'is-array';
const OBJECT = 'object';
export const DEEP = 1;
export const SHALLOW = 2;
/**
 * Clone the input removing any reference
 * @param {*} input
 * @return mixed
 */
function clone(input) {
	let output = input;
	if (isArray(input)) {
		output = input.map(clone);
	} else if (typeof input === OBJECT) {
		output = input ? {} : input;
		for (let key in input) {
			output[key] = clone(input[key]);
		}
	}
	return output;
}
/**
 * Merge two objects recursively
 * @param {*} base
 * @param {*} extend
 * @return {*}
 */
function mergeRecursive(base, extend) {
	if (!base || typeof base !== OBJECT) {
		base = {};
	}
	for (let key in extend) {
		let baseVal = base[key],
			extendVal = extend[key];
		if (typeof baseVal === OBJECT && typeof extendVal === OBJECT) {
			if (isArray(baseVal) || isArray(extendVal)) {
				base[key] = extendVal;
			} else {
				base[key] = mergeRecursive(baseVal, extendVal);
			}
		} else {
			base[key] = extendVal;
		}
	}
	return base;
}
/**
 * Merge two or more objects. DO NOT USE AN ARROW FUNCTION HERE. We need acces to arguments
 * @param {Number} [type]
 * @param {...?Object} rest
 * @return object
 */
const cloneMerge = function (type) {
	let returning = {},
		deepMerge = false,
		shallow = false,
		index = 0;
	if (typeof type === 'number') {
		index = 1;
		deepMerge = type & DEEP;
		shallow = type & SHALLOW;
	}

	for (let c = arguments.length; index < c; index++) {
		let item = arguments[index];
		if (!item || typeof item !== OBJECT) {
			continue;
		}
		for (let key in item) {
			let objVal = item[key];
			if (objVal !== undefined) {
				let val = shallow ? objVal : clone(objVal);
				if (
					deepMerge &&
					typeof returning[key] === OBJECT &&
					typeof val === OBJECT &&
					!isArray(val)
				) {
					returning[key] = mergeRecursive(clone(returning[key]), val);
				} else {
					returning[key] = val;
				}
			}
		}
	}
	return returning;
};

export default cloneMerge;

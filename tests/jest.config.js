const aliases = require('../config/aliases');

module.exports = {
    coverageDirectory: '<rootDir>/coverage',
    collectCoverage: true,
    moduleNameMapper: {
        ...aliases.forJest
    },
    coverageReporters: ['cobertura'],
    collectCoverageFrom: ['<rootDir>/src/**/*.{js,jsx}'],
    testPathIgnorePatterns: ['tests/setup'],
    globals: {
        __PATH_PREFIX__: ''
    },
    testURL: 'http://localhost',
    rootDir: '../'
};
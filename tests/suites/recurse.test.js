import assert from 'assert';
import lib, { DEEP, SHALLOW } from '#root';
describe('Recurse', () => {
    it('merge default', () => {

        let rec = lib({
            x: {
                y: 1,
                z: 3
            }
        }, {
            x: {
                y: 2
            }
        });

        expect(rec).toEqual({
            x: {
                y: 2
            }
        });
    });

    it('merge deeply', () => {

        let rec = lib(DEEP, {
            x: {
                y: 1,
                z: 3
            }
        }, {
            x: {
                y: 2
            }
        });

        expect(rec).toEqual({
            x: {
                y: 2,
                z: 3
            }
        });
    });

    it('merge shallow', () => {

        let x = {
            foo: 'bar'
        };
        let rec = lib(SHALLOW, {
            x
        }, {
            q: {
                y: 2
            }
        });

        assert(rec.x === x);

        expect(rec).toEqual({
            x,
            q: {
                y: 2
            }
        });

    });

    it('merges null correctly', () => {

        let x = {
            foo: 'bar'
        };
        let rec = lib({
            x
        }, {
            q: null
        });


        expect(rec).toMatchObject({
            x,
            q: null
        });

    });

});